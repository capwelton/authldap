## Athentification LDAP ##

Authentification LDAP ou AD.

Permet de spécifier plusieurs serveurs LDAP ou AD, et de configurer un ordre d'interrogation des serveurs pour une authentification ou une synchronisation.

### Changelog ###

####Version 1.0.4####

* Correction de l'authentification ne fonctionnant pas sur des serveurs UNIX

####Version 1.0.3####

* Au choix de la méthode d'authenfication dans les options du site, la case "Permettre aux administrateurs de se connecter si l'authentification par LDAP échoue" est automatiquement cochée
* Changement du nom des sections dans les paramètres de synchronisation d'un serveur d'authentification
* Ajouts de traduction
* Correction de la redirection vers une page blanche lors de la sauvegarde d'un serveur d'authentification
* Ajout du lien "Lancer la synchronisation dans une fenêtre popup" en bas du formulaire d'édition d'un serveur d'authentification
* Ajout d'un champ "Port" dans le formulaire d'édition d'un serveur d'authentificaiton
* Dans le cas d'un serveur Active Directory, mapper automatiquement le champ "samaccountname" sur le login de l'utilisateur
* Correction de la synchronisation des utilisateurs d'un serveur ne s'effectuant pas
* Afficher dans les logs de synchronisation lorsqu'un utilisateur a été mis à jour
* Afficher un message d'information quant à l'utilisation du champ "Désactiver les utilisateurs Ovidentia non présent sur le serveur LDAP" si plusieurs serveurs LDAP/AD sont configurés
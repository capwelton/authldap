<?php

/************************************************************************
 * COFY https://www.siforyou.com                                         *
 ************************************************************************
 * Copyright (c) 2023 by Si-4You ( https://www.siforyou.com )            *
 *                                                                      *
 * This file is part of Cofy.                                           *
 *                                                                      *
 * Cofy is free software; you can redistribute it and/or modify         *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.                                                   *
 *                                                                      *
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                 *
 * See the  GNU General Public License for more details.                *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program; if not, write to the Free Software          *
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.                                                                 *
 ************************************************************************/
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2024 by SI-4YOU ({@link https://www.siforyou.com})
 */

use Capwelton\LibOrm\MySql\ORMMySqlBackend;
use Cofy\Utilities\Database\BabSynchronizeSql;
use Cofy\Utilities\FunctionalityTools\BabFunctionalities;
use Cofy\Utilities\FunctionalityTools\BabFunctionality;
use Cofy\Utilities\Iterator\BabPath;

use function Cofy\Utilities\Addon\bab_getAddonInfosInstance;
use function Cofy\Utilities\Addon\bab_getDB;

require_once 'base.php';
require_once dirname(__FILE__) . '/functions.php';

/**
 * Fonction appellée au moment de la mise à jour et de l'installation de l'addon
 */
function authldap_upgrade($version_base,$version_ini)
{
    global $babDB;
    
    $serverSet = authldap_ServerSet();
    $siteLinkSet = authldap_SiteLinkSet();
    $userSet = authldap_UserSet();
    $optionSet = authldap_SiteOptionSet();
    $notifiedGroupsSet = authldap_NotifiedGroupsSet();
	$syncLogSet = authldap_SyncLogSet();
	$userSyncSet = authldap_UserSyncSet();
    
	$synchronize = new BabSynchronizeSql();
	$mysqlBackend = new ORMMySqlBackend(bab_getDB());
	$sql = "";
	$sql .= $mysqlBackend->setToSql($serverSet)."\n";
	$sql .= $mysqlBackend->setToSql($siteLinkSet)."\n";
	$sql .= $mysqlBackend->setToSql($userSet)."\n";
	$sql .= $mysqlBackend->setToSql($optionSet)."\n";
	$sql .= $mysqlBackend->setToSql($notifiedGroupsSet)."\n";
	$sql .= $mysqlBackend->setToSql($syncLogSet)."\n";
	$sql .= $mysqlBackend->setToSql($userSyncSet)."\n";

	$synchronize->fromSqlString($sql);

	if(!isset($version_base) || empty($version_base)){
	    //If this is an installation, we copy the server configuration from the site configuration
	    $status = authldap_copyServerFromSite();
        foreach ($status as $msg){
            echo $msg;
        }
	}
	
	$addon = bab_getAddonInfosInstance('authldap');
	$func = new BabFunctionalities();
	require_once dirname(__FILE__). '/authentication.class.php';
	$func->register('PortalAuthentication/AuthLdap');
	$addon->removeAllEventListeners();
	$addon->addEventListener('LibTimer_eventHourly', 'authldap_onHourly', 'events.php');

	$defaultLogPath = $addon->getUploadPath().'/log';
	$defaultLogPath = new BabPath($defaultLogPath);
	$defaultLogPath->createDir();

	return true;
}


/**
 * Fonction appellée au moment de la suppression de l'addon
 */
function authldap_onDeleteAddon()
{
    global $babDB;
    
    $AuthLdap = BabFunctionality::get('PortalAuthentication/AuthLdap');
//     $authldapAuthType = $AuthLdap->getAuthenticationType();
    
    $addon = bab_getAddonInfosInstance('authldap');
    $addon->unregisterFunctionality('PortalAuthentication/AuthLdap');
	$addon->removeAllEventListeners();
	
	$AuthOvidentia = BabFunctionality::get('PortalAuthentication/AuthCofy');
	$ovidentiaAuthType = $AuthOvidentia->getAuthenticationType();
	
	$sitesQuery = "SELECT id FROM ".BAB_SITES_TBL." WHERE authentification=".$babDB->quote($authldapAuthType);
	$sitesResults = $babDB->db_query($sitesQuery);
	if( $babDB->db_num_rows($sitesResults) > 0 )
	{
	    while($site = $babDB->db_fetch_assoc($sitesResults))
	    {
	        $query = "UPDATE ".BAB_SITES_TBL." SET authentification=".$babDB->quote($ovidentiaAuthType)." WHERE id=".$babDB->quote($site['id']);
	        $babDB->db_query($query);
	    }
	}
		
	return true;
}
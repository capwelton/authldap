<?php

/************************************************************************
 * COFY https://www.siforyou.com                                         *
 ************************************************************************
 * Copyright (c) 2023 by Si-4You ( https://www.siforyou.com )            *
 *                                                                      *
 * This file is part of Cofy.                                           *
 *                                                                      *
 * Cofy is free software; you can redistribute it and/or modify         *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.                                                   *
 *                                                                      *
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                 *
 * See the  GNU General Public License for more details.                *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program; if not, write to the Free Software          *
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.                                                                 *
 ************************************************************************/
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2024 by SI-4YOU ({@link https://www.siforyou.com})
 */

use Capwelton\Widgets\Widgets\Form\WidgetForm;
use Cofy\Utilities\Func\Icons\FuncIcons;

use function Cofy\Utilities\Addon\bab_nbsp;
use function Cofy\Utilities\Addon\bab_rp;
use function Cofy\Utilities\Addon\bab_Widgets;

include_once 'base.php';
bab_Widgets()->includePhpClass('Widget_Form');

class authldap_ConfigurationEditor extends WidgetForm
{

    private $servers;

    public function __construct($servers)
    {
        $W = bab_Widgets();

        parent::__construct(null, $W->VBoxLayout()->setVerticalSpacing(1, 'em'));

        $this->servers = $servers;

        $this->setHiddenValue('tg', bab_rp('tg'));
        $this->setName('serverOrder');
        $this->colon();
        $this->addClass(FuncIcons::ICON_LEFT_16);

        $this->addItems();
        $this->addButtons();
    }

    protected function addItems()
    {
        $this->addItem($this->serverOptions());
        $this->addItem($this->createServerLink());
        $this->addItem($this->sortServers());
    }

    protected function addButtons()
    {
        $W = bab_Widgets();

        $button = $W->FlowItems(
            $W->SubmitButton()
                ->setAjaxAction(authldap_Controller()->Admin()->saveOrder())
                ->setName('save')->setLabel(authldap_translate('Save'))
        )->setSpacing(1, 'em');

        $this->addItem($button);
    }

    protected function createServerLink()
    {
        $W = bab_Widgets();
        return $W->Link(
            authldap_translate('Create a new server'),
            authldap_Controller()->Server()->edit()
        )->addClass('icon ' . FuncIcons::ACTIONS_LIST_ADD);
    }

    protected function sortServers()
    {
        $W = bab_Widgets();
        $set = authldap_SiteLinkSet();
        $serversBox = $W->VBoxItems()->addClass(FuncIcons::ICON_LEFT_16)->sortable(true);
        foreach ($this->servers as $server) {
            $serverLink = $set->get($set->server->is($server->id));
            $rank = isset($serverLink->rank) ? $serverLink->rank : authldap_translate('Unused server');
            $used = isset($serverLink->used) ? $serverLink->used : 0;
            $rank = authldap_translate('Unused server');
            if ($used && isset($serverLink->rank)) {
                $rank = $serverLink->rank;
            }
            $serversBox->addItems(
                $W->HBoxItems(
                    $W->Link('', authldap_Controller()->Server()->edit($server->id))->addClass('icon ' . FuncIcons::ACTIONS_DOCUMENT_EDIT)->setTitle(authldap_translate('Edit server')),
                    $W->LabelledWidget(
                        $rank . '.' . bab_nbsp() . $server->name,
                        $W->FlowItems(
                            $W->Hidden()->setName(array('order', $server->id))->setValue($server->id),
                            $W->LabelledWidget(
                                authldap_translate('Use this server'),
                                $W->CheckBox()->setName(array('used', $server->id))->setValue((bool)$used)
                            )
                        )
                    )
                )->setVerticalAlign('middle')
            );
        }

        return $W->Section(authldap_translate('Servers order'), $serversBox)->setFoldable(true);
    }

    protected function serverOptions()
    {
        $W = bab_Widgets();
        $box = $W->VBoxItems();

        $box->addItems(
            $W->LabelledWidget(
                authldap_translate('Allow administrators to log in if the LDAP authentication fails'),
                $W->CheckBox()->setName('allowAdmin')
            ),
            $W->LabelledWidget(
                authldap_translate('Notify selected groups when a new user is registered'),
                $W->CheckBox()->setName('notifyGroups')
            ),
            $W->LabelledWidget(
                authldap_translate('Test user\'s fields validy on login (lastname, firstname, email)'),
                $W->CheckBox()->setName('checkFields')
            ),
            $W->Link(authldap_translate('Show selected groups'), authldap_Controller()->Admin()->notifiedGroups())
        );


        return $W->Section(authldap_translate('Options'), $box)->setFoldable(true);
    }
}

class authldap_UserEditor extends WidgetForm
{
    protected $user;

    public function __construct($user, $pos, $grp)
    {
        $W = bab_Widgets();

        parent::__construct(null, $W->VBoxLayout()->setVerticalSpacing(2, 'em'));

        $this->user = $user;

        $this->setHiddenValue('tg', 'addon/authldap/main');
        $this->setHiddenValue('pos', $pos);
        $this->setHiddenValue('grp', $grp);
        $this->setHiddenValue('user[user]', $user);
        $this->setName('user');
        $this->colon();

        $this->addItems();
        $this->addButtons();

        $this->loadValues();
    }

    public function addItems()
    {
        $W = bab_Widgets();

        $this->addItem(
            $W->LabelledWidget(
                authldap_translate('Allow local authentication'),
                $W->CheckBox(),
                'allow'
            )
        );
    }

    public function addButtons()
    {
        $W = bab_Widgets();
        $this->addItem(
            $W->SubmitButton()->setAction(authldap_Controller()->Admin()->saveUser())
        );
    }

    protected function loadValues()
    {
        $set = authldap_UserSet();
        $user = $set->get($set->user->is($this->user));
        if ($user) {
            $this->setValue(array('user', 'allow'), $user->allow);
        }
    }
}

class authldap_NotifiedGroupsEditor extends WidgetForm
{
    public function __construct()
    {
        global $babBody;
        $W = bab_Widgets();


        parent::__construct(null, $W->VBoxLayout()->setVerticalSpacing(2, 'em'));

        $this->setHiddenValue('tg', 'addon/authldap/main');
        $this->addItem($W->Hidden()->setName('site')->setValue($babBody->babsite['id']));
        $this->setName('notifiedGroups');
        $this->colon();

        $this->addItems();
        $this->addButtons();
    }

    public function addItems()
    {
        global $babBody;

        $W = bab_Widgets();

        $acl = $W->Acl();
        $acl->setTitle(authldap_translate('Who is notified when an account is created when a user first loggin on a LDAP directory or active directory or when a new user register?'));
        $acl->setName('groups');

        $this->addItem($acl);
    }

    public function addButtons()
    {
        $W = bab_Widgets();
        $this->addItem(
            $W->SubmitButton()->setAction(authldap_Controller()->Admin()->saveNotifiedGroups())
        );
    }
}

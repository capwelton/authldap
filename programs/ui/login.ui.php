<?php

/************************************************************************
 * COFY https://www.siforyou.com                                         *
 ************************************************************************
 * Copyright (c) 2023 by Si-4You ( https://www.siforyou.com )            *
 *                                                                      *
 * This file is part of Cofy.                                           *
 *                                                                      *
 * Cofy is free software; you can redistribute it and/or modify         *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.                                                   *
 *                                                                      *
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                 *
 * See the  GNU General Public License for more details.                *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program; if not, write to the Free Software          *
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.                                                                 *
 ************************************************************************/
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2024 by SI-4YOU ({@link https://www.siforyou.com})
 */

use function Cofy\Utilities\Addon\bab_getAddonInfosInstance;
use function Cofy\Utilities\Addon\bab_rp;
use function Cofy\Utilities\Addon\bab_toHtml;
use function Cofy\Utilities\Addon\bab_translate;
use function Cofy\Utilities\bab_GetSettingsInstance;
use function Cofy\Utilities\User\bab_isEmailPassword;

require_once 'base.php';
require_once dirname(__FILE__) . '/../functions.php';
class authldap_displayLogin_Template
{
	public $email;
	public $password;
	public $login;
	public $nickname;
	public $referer;
	public $life;
	public $nolife;
	public $oneday;
	public $oneweek;
	public $onemonth;
	public $oneyear;
	public $infinite;
	public $emailPwdUrl;
	public $emailPwdTxt;
	public $c_nickname;

	public function __construct($url)
	{
		$this->email = bab_translate("Email");
		$this->password = bab_translate("Password");
		$this->login = bab_translate("Login");
		$this->nickname = bab_translate("Login");

		// verify and buid url
		$params = array();
		$arr = explode('?', $url);

		if (isset($arr[1])) {
			$params = explode('&', $arr[1]);
		}

		$url = $GLOBALS['babPhpSelf'];

		foreach ($params as $key => $param) {
			$arr = explode('=', $param);
			if (2 == count($arr)) {

				$params[$key] = $arr[0] . '=' . $arr[1];
			} else {
				unset($params[$key]);
			}
		}

		if (0 < count($params)) {
			$url .= '?' . implode('&', $params);
		}

		$url = str_replace("\n", '', $url);
		$url = str_replace("\r", '', $url);
		$url = str_replace('%0d', '', $url);
		$url = str_replace('%0a', '', $url);

		$this->referer = bab_toHtml($url);
		$this->life = bab_translate("Connection timeout");
		$this->nolife = bab_translate("No");
		$this->oneday = bab_translate("one day");
		$this->oneweek = bab_translate("one week");
		$this->onemonth = bab_translate("one month");
		$this->oneyear = bab_translate("one year");
		$this->infinite = bab_translate("unlimited");

		$this->emailPwdUrl = bab_toHtml('?tg=login&cmd=emailpwd');
		$this->emailPwdTxt = bab_translate('Lost Password');

		$this->c_nickname = isset($_COOKIE['c_nickname']) ? bab_toHtml($_COOKIE['c_nickname']) : '';
	}
}

function authldap_Error($msg)
{
	global $babBody;

	$babBody->addError($msg);
	$babBody->babEcho(sprintf('<a href="%s">%s</a>', bab_toHtml($GLOBALS['babUrlScript'] . '?tg=login&cmd=signon&sAuthType=Ovidentia'), authldap_translate('Continue')));
}

/**
 * This form allow email login but no registration of new account
 *
 */
function authldap_displayLoginForm()
{
	global $babBody;

	$title = bab_rp('msg', '');
	$errorMessages = bab_rp('err', '');

	$babBody->setTitle($title);
	$errors = explode("\n", $errorMessages);
	foreach ($errors as $errorMessage) {
		$babBody->addError($errorMessage);
	}
	$babBody->addItemMenu('signon', bab_translate("Login"), '');

	$settings = bab_GetSettingsInstance();
	/*@var $settings bab_Settings */
	$site = $settings->getSiteSettings();

	if ($site['registration'] == 'Y') {
		$babBody->addItemMenu('register', bab_translate("Register"), $GLOBALS['babUrlScript'] . '?tg=login&cmd=register');
	}

	if (bab_isEmailPassword()) {
		$babBody->addItemMenu('emailpwd', bab_translate("Lost Password"), $GLOBALS['babUrlScript'] . '?tg=login&cmd=emailpwd');
	}

	$temp = new authldap_displayLogin_Template(bab_rp('referer'));
	$addon = bab_getAddonInfosInstance('authldap');
	$babBody->babecho($addon->printTemplate($temp, 'authldap.login.html', 'login'));
}

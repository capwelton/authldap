<?php

/************************************************************************
 * COFY https://www.siforyou.com                                         *
 ************************************************************************
 * Copyright (c) 2023 by Si-4You ( https://www.siforyou.com )            *
 *                                                                      *
 * This file is part of Cofy.                                           *
 *                                                                      *
 * Cofy is free software; you can redistribute it and/or modify         *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.                                                   *
 *                                                                      *
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                 *
 * See the  GNU General Public License for more details.                *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program; if not, write to the Free Software          *
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.                                                                 *
 ************************************************************************/
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2024 by SI-4YOU ({@link https://www.siforyou.com})
 */

use Capwelton\LibOrm\Criteria\ORMCriteria;
use Capwelton\LibOrm\Field\ORMBoolField;
use Capwelton\LibOrm\Field\ORMDatetimeField;
use Capwelton\LibOrm\Field\ORMIntField;
use Capwelton\LibOrm\Field\ORMStringField;
use Capwelton\LibOrm\Field\ORMTextField;
use Capwelton\LibOrm\MySql\ORMMySqlRecordSet;
use Capwelton\LibOrm\MySql\ORMMySqlBackend;
use Capwelton\LibOrm\ORMIterator;
use Capwelton\LibOrm\ORMRecordSet;
use Capwelton\LibOrm\ORMRecord;
use Cofy\Utilities\Addon\BabCharset;
use Cofy\Utilities\FunctionalityTools\BabFunctionality;

use function Capwelton\LibOrm\ORM_StringField;
use function Capwelton\LibOrm\ORM_IntField;
use function Capwelton\LibOrm\ORM_BoolField;
use function Capwelton\LibOrm\ORM_DatetimeField;
use function Capwelton\LibOrm\ORM_TextField;
use function Cofy\Utilities\Dir\bab_getDirectoriesFields;

require_once "base.php";
require_once dirname(__FILE__) . '/../functions.php';

global $babDB;

/** @var FuncLibOrm $LibOrm */
$LibOrm = BabFunctionality::get('LibOrm');

$LibOrm->initMysql();
ORMMySqlRecordSet::setBackend(new ORMMySqlBackend($babDB));

/**
 * @method authldap_Server[]|ORMIterator select(ORMCriteria $criteria)
 * @method authldap_Server get(mixed $criteria)
 * @method authldap_Server request(mixed $criteria)
 * @method authldap_Server newRecord()
 * 
 * @property ORMStringField $name
 * @property ORMIntField    $serverType
 * @property ORMBoolField   $active
 * @property ORMStringField $serverAddress
 * @property ORMStringField $serverPort
 * @property ORMStringField $searchBase
 * @property ORMStringField $passwordEncodeType
 * @property ORMStringField $administratorDN
 * @property ORMStringField $administratorPassword
 * @property ORMStringField $userDN
 * @property ORMStringField $domainName
 * @property ORMStringField $filter
 * @property ORMStringField $serverEncodeType
 * @property ORMStringField $login
 * @property ORMBoolField   $sync
 * @property ORMStringField $sync_filter
 * @property ORMDatetimeField   $last_synchronization
 * @property ORMBoolField   $timer_active
 * @property ORMStringField $timer_hour
 * @property ORMStringField $logfile
 * @property ORMStringField $password
 * @property ORMBoolField   $is_confirmed
 * @property ORMBoolField   $disableusers
 * @property ORMBoolField   $neverdisableadmin
 * @property ORMBoolField   $neverdisableauth
 * @property ORMStringField $defaultemail
 * @property ORMStringField $userfield
 * @property ORMIntField    $root
 * @property ORMBoolField   $create_groups
 * @property ORMBoolField   $remove_from_group
 * @property ORMBoolField   $remove_from_groups_on_disable
 * @property ORMStringField $group_path_method
 * @property ORMStringField $groupsattribute
 * @property ORMIntField    $grouproot
 * @property ORMTextField   $recipients
 * @property ORMStringField $sync_login
 * @property ORMStringField $sync_password
 * @property ORMStringField $sn
 * @property ORMStringField $givenname
 * @property ORMStringField $jpegphoto
 * @property ORMStringField $email
 * @property ORMStringField $btel
 * @property ORMStringField $mobile
 * @property ORMStringField $bfax
 * @property ORMStringField $title
 * @property ORMStringField $departmentnumber
 * @property ORMStringField $organisationname
 * @property ORMStringField $bstreetaddress
 * @property ORMStringField $bcity
 * @property ORMStringField $bpostalcode
 * @property ORMStringField $bcountry
 * 
 * 
 * @inheritdoc  ORMRecordSet
 * @see         ORMRecordSet
 */
class authldap_ServerSet extends ORMRecordSet
{

    const SERVER_LDAP = 0;
    const SERVER_AD = 1;

    public function __construct()
    {
        parent::__construct();

        $this->setPrimaryKey('id');

        $this->addFields(
            ORM_StringField('name'),
            ORM_IntField('serverType')->setDescription('The server type. 0 for LDAP, 1 for AD'),
            ORM_BoolField('active')->setDescription('Wether the server is active or not'),
            ORM_StringField('serverAddress'),
            ORM_StringField('serverPort'),
            ORM_StringField('searchBase'),
            ORM_StringField('passwordEncodeType')->setDescription('Only used when server type is LDAP'),
            ORM_StringField('administratorDN')->setDescription('Only used when server type is LDAP'),
            ORM_StringField('administratorPassword')->setDescription('Only used when server type is LDAP'),
            ORM_StringField('userDN')->setDescription('Only used when server type is LDAP'),
            ORM_StringField('domainName')->setDescription('Only used when server type is AD'),
            ORM_StringField('filter'),
            ORM_StringField('serverEncodeType'),
            ORM_StringField('login'),

            //SYNCHRO PARAMETERS
            ORM_BoolField('sync')->setDescription('Wether the server is synchronised or not'),
            ORM_StringField('sync_filter'),
            ORM_DatetimeField('last_synchronization'),

            ORM_BoolField('timer_active'),
            ORM_StringField('timer_hour'),
            ORM_StringField('logfile'),
            ORM_StringField('password'),
            ORM_BoolField('is_confirmed'),
            ORM_BoolField('disableusers'),
            ORM_BoolField('neverdisableadmin'),
            ORM_BoolField('neverdisableauth'),
            ORM_StringField('defaultemail'),

            ORM_StringField('userfield'),
            ORM_IntField('root'),
            ORM_BoolField('create_groups'),
            ORM_BoolField('remove_from_group'),
            ORM_BoolField('remove_from_groups_on_disable'),
            ORM_StringField('group_path_method'),

            ORM_StringField('groupsattribute'),
            ORM_IntField('grouproot'),

            ORM_TextField('recipients'),

            ORM_StringField('sync_login')->setDescription('admin login of the server to execute the synchronization'),
            ORM_StringField('sync_password')->setDescription('admin password of the server to execute the synchronization')
        );

        //MAPPING
        $dirFields = bab_getDirectoriesFields(array(0));
        foreach ($dirFields as $dirField) {
            $this->addFields(
                ORM_StringField($dirField['name'])
            );
        }
    }

    public function getServerOrder($siteId = 1)
    {
        $siteLinkSet = authldap_SiteLinkSet();
        $siteLinks = $siteLinkSet->select($siteLinkSet->site->is($siteId))->orderAsc($siteLinkSet->rank);

        $servers = array();
        $serversId = array();
        foreach ($siteLinks as $siteLink) {
            $servers[] = $siteLink->server();
            $serversId[] = $siteLink->server()->id;
        }

        $serverSet = authldap_ServerSet();
        $otherServers = $serverSet->select($serverSet->id->notIn($serversId));
        foreach ($otherServers as $otherServer) {
            $servers[] = $otherServer;
        }

        return $servers;
    }
}


/**
 * @method authldap_ServerSet   getParentSet()
 * 
 * @property string $name
 * @property int    $serverType
 * @property bool   $active
 * @property string $serverAddress
 * @property string $serverPort
 * @property string $searchBase
 * @property string $passwordEncodeType
 * @property string $administratorDN
 * @property string $administratorPassword
 * @property string $userDN
 * @property string $domainName
 * @property string $filter
 * @property string $serverEncodeType
 * @property string $login
 * @property bool   $sync
 * @property string $sync_filter
 * @property string   $last_synchronization
 * @property bool   $timer_active
 * @property string $timer_hour
 * @property string $logfile
 * @property string $password
 * @property bool   $is_confirmed
 * @property bool   $disableusers
 * @property bool   $neverdisableadmin
 * @property bool   $neverdisableauth
 * @property string $defaultemail
 * @property string $userfield
 * @property int    $root
 * @property bool   $create_groups
 * @property bool   $remove_from_group
 * @property bool   $remove_from_groups_on_disable
 * @property string $group_path_method
 * @property string $groupsattribute
 * @property int    $grouproot
 * @property string $recipients
 * @property string $sync_login
 * @property string $sync_password
 * @property string $sn
 * @property string $givenname
 * @property string $jpegphoto
 * @property string $email
 * @property string $btel
 * @property string $mobile
 * @property string $bfax
 * @property string $title
 * @property string $departmentnumber
 * @property string $organisationname
 * @property string $bstreetaddress
 * @property string $bcity
 * @property string $bpostalcode
 * @property string $bcountry
 * 
 * @inheritdoc  ORMRecord
 * @see         ORMRecord
 */
class authldap_Server extends ORMRecord
{
    public function ldapEncode($str, $type = null)
    {
        global $babBody;
        $ovCharset = BabCharset::getDatabase();

        if (null === $type && isset($this->serverEncodeType)) {
            $type = $this->serverEncodeType;
        }
        $type = (int) $type;

        switch ($type) {
            case BAB_LDAP_UTF8:
                if ('utf8' === $ovCharset) {
                    return $str; // utf8 to utf8
                } else {
                    return mb_convert_encoding($str, 'UTF-8');
                }
                break;

            case BAB_LDAP_ISO8859:
                if ('utf8' === $ovCharset) {
                    return mb_convert_encoding($str, 'UTF-8');
                } else {
                    return $str; // latin1 to latin1
                }
                break;

            default:
                trigger_error('Unsupported Charset');
                return $str;
                break;
        }
    }

    public function setValues(array $values)
    {
        parent::setValues($values);
        foreach ($values as $fieldName => $mixedValue) {
            if ($values[$fieldName] == 'other') {
                if (isset($values[$fieldName . 'Value']) && !empty($values[$fieldName . 'Value'])) {
                    $this->$fieldName = $values[$fieldName . 'Value'];
                }
            }
        }
        return $this;
    }

    public function ldapDecode($str, $type = null)
    {
        $ovCharset = BabCharset::getDatabase();

        if (null === $type && isset($this->serverEncodeType)) {
            $type = $this->serverEncodeType;
        }


        $type = (int) $type;

        switch ($type) {
            case BAB_LDAP_UTF8:
                if ('utf8' === $ovCharset) {
                    return $str; // utf8 to utf8
                } else {
                    return mb_convert_encoding($str, 'UTF-8');
                }
                break;

            case BAB_LDAP_ISO8859:

                if ('utf8' === $ovCharset) {
                    return mb_convert_encoding($str, 'UTF-8');
                } else {
                    return $str; // latin1 to latin1
                }
                break;

            default:
                trigger_error('Unsupported Charset');
                return $str;
                break;
        }
    }

    public function performSynchronization(LibTimer_eventHourly $event = null)
    {
        require_once dirname(__FILE__) . '/../syncincl.php';
        ini_set('max_execution_time', '1200');

        $syncLogSet = authldap_SyncLogSet();
        $syncLog = $syncLogSet->newRecord();
        $syncLog->server = $this->id;
        $syncLog->createdOn = ORMDatetimeField::getCurrentDateTime();
        $syncLog->status = authldap_SyncLog::SYNCLOG_BEGIN;
        $syncLog->save();

        $sync = new authldap_Sync($this, $syncLog, $event);
        $sync->processEntries();
        $sync->close();

        return $syncLog;
    }

    public function getTypeName()
    {
        switch ($this->type) {
            case authldap_ServerSet::SERVER_AD:
                return 'AD';
            case authldap_ServerSet::SERVER_LDAP:
                return 'LDAP';
            default:
                return authldap_translate('Unknown');
        }
    }

    public function getAddressName()
    {
        $name = '';

        if (isset($this->serverAddress)) {
            $name .= $this->serverAddress;
        }
        if (isset($this->serverPort)) {
            $name .= ':' . $this->serverPort;
        }

        return $name;
    }
}

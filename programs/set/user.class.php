<?php

/************************************************************************
 * COFY https://www.siforyou.com                                         *
 ************************************************************************
 * Copyright (c) 2023 by Si-4You ( https://www.siforyou.com )            *
 *                                                                      *
 * This file is part of Cofy.                                           *
 *                                                                      *
 * Cofy is free software; you can redistribute it and/or modify         *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.                                                   *
 *                                                                      *
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                 *
 * See the  GNU General Public License for more details.                *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program; if not, write to the Free Software          *
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.                                                                 *
 ************************************************************************/
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2024 by SI-4YOU ({@link https://www.siforyou.com})
 */

use Capwelton\LibOrm\Criteria\ORMCriteria;
use Capwelton\LibOrm\Field\ORMBoolField;
use Capwelton\LibOrm\Field\ORMUserField;
use Capwelton\LibOrm\FuncLibOrm;
use Capwelton\LibOrm\MySql\ORMMySqlRecordSet;
use Capwelton\LibOrm\MySql\ORMMySqlBackend;
use Capwelton\LibOrm\ORMIterator;
use Capwelton\LibOrm\ORMRecordSet;
use Capwelton\LibOrm\ORMRecord;
use Cofy\Utilities\FunctionalityTools\BabFunctionality;

use function Capwelton\LibOrm\ORM_UserField;
use function Capwelton\LibOrm\ORM_BoolField;

require_once "base.php";
require_once dirname(__FILE__) . '/../functions.php';

global $babDB;

/** @var FuncLibOrm $LibOrm */
$LibOrm = BabFunctionality::get('LibOrm');

$LibOrm->initMysql();
ORMMySqlRecordSet::setBackend(new ORMMySqlBackend($babDB));

/**
 * @method authldap_User[]|ORMIterator select(ORMCriteria $criteria)
 * @method authldap_User    get(mixed $criteria)
 * @method authldap_User    request(mixed $criteria)
 * @method authldap_User    newRecord()
 * 
 * @property ORMUserField   $user
 * @property ORMBoolField   $allow
 * 
 * @inheritdoc  ORMRecordSet
 * @see         ORMRecordSet
 */
class authldap_UserSet extends ORMRecordSet
{

    public function __construct()
    {
        parent::__construct();

        $this->setPrimaryKey('id');

        $this->addFields(
            ORM_UserField('user'),
            ORM_BoolField('allow')
        );
    }
}

/**
 * @method authldap_UserSet getParentSet()
 * 
 * @property int    $user
 * @property bool   $allow
 * 
 * @inheritdoc  ORMRecord
 * @see         ORMRecord
 */
class authldap_User extends ORMRecord {}

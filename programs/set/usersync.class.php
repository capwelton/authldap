<?php

/************************************************************************
 * COFY https://www.siforyou.com                                         *
 ************************************************************************
 * Copyright (c) 2023 by Si-4You ( https://www.siforyou.com )            *
 *                                                                      *
 * This file is part of Cofy.                                           *
 *                                                                      *
 * Cofy is free software; you can redistribute it and/or modify         *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.                                                   *
 *                                                                      *
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                 *
 * See the  GNU General Public License for more details.                *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program; if not, write to the Free Software          *
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.                                                                 *
 ************************************************************************/
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2024 by SI-4YOU ({@link https://www.siforyou.com})
 */

use Capwelton\LibOrm\Criteria\ORMCriteria;
use Capwelton\LibOrm\Field\ORMEnumField;
use Capwelton\LibOrm\Field\ORMFkField;
use Capwelton\LibOrm\FuncLibOrm;
use Capwelton\LibOrm\MySql\ORMMySqlRecordSet;
use Capwelton\LibOrm\MySql\ORMMySqlBackend;
use Capwelton\LibOrm\ORMIterator;
use Capwelton\LibOrm\ORMRecordSet;
use Capwelton\LibOrm\ORMRecord;
use Cofy\Utilities\FunctionalityTools\BabFunctionality;

use function Capwelton\LibOrm\ORM_UserField;
use function Capwelton\LibOrm\ORM_EnumField;

require_once "base.php";
require_once dirname(__FILE__) . '/../functions.php';

global $babDB;

/** @var FuncLibOrm $LibOrm */
$LibOrm = BabFunctionality::get('LibOrm');

$LibOrm->initMysql();
ORMMySqlRecordSet::setBackend(new ORMMySqlBackend($babDB));

/**
 * @method authldap_UserSync[]|ORMIterator select(ORMCriteria $criteria)
 * @method authldap_UserSync    get(mixed $criteria)
 * @method authldap_UserSync    request(mixed $criteria)
 * @method authldap_UserSync    newRecord()
 * @method authldap_SyncLogSet  syncLog()
 * 
 * @property ORMUserField   $user
 * @property ORMEnumField   $typeOfAction
 * @property ORMFkField     $syncLog
 * 
 * @inheritdoc  ORMRecordSet
 * @see         ORMRecordSet
 */
class authldap_UserSyncSet extends ORMRecordSet
{

    public function __construct()
    {
        parent::__construct();

        $this->setPrimaryKey('id');

        $this->addFields(
            ORM_UserField('user'),
            ORM_EnumField('typeOfAction', authldap_UserSync::getTypesOfAction())
        );
        $this->hasOne('syncLog', 'authldap_SyncLogSet')->setDescription('The synchronisation log id');
    }
}

/**
 * @method authldap_UserSyncSet getParentSet()
 * @method authldap_SyncLog     syncLog()
 * 
 * @property int    $user
 * @property int    $typeOfAction
 * @property int    $syncLog
 * 
 * @inheritdoc  ORMRecord
 * @see         ORMRecord
 */
class authldap_UserSync extends ORMRecord
{
    const TYPE_ACTION_CREATE = 0;
    const TYPE_ACTION_UPDATE = 1;
    const TYPE_ACTION_DISABLE = 2;

    public static function getTypesOfAction()
    {
        return [
            self::TYPE_ACTION_CREATE => authldap_translate('Create'),
            self::TYPE_ACTION_UPDATE => authldap_translate('Update'),
            self::TYPE_ACTION_DISABLE => authldap_translate('Disable')
        ];
    }

    public function getTypeOfAction()
    {
        switch ($this->typeOfAction) {
            case self::TYPE_ACTION_CREATE:
                return  authldap_translate('Create');
                break;
            case self::TYPE_ACTION_UPDATE:
                return  authldap_translate('Update');
                break;
            case self::TYPE_ACTION_DISABLE:
                return  authldap_translate('Disable');
                break;
        }
        return authldap_translate("type of action");
    }
}

<?php

/************************************************************************
 * COFY https://www.siforyou.com                                         *
 ************************************************************************
 * Copyright (c) 2023 by Si-4You ( https://www.siforyou.com )            *
 *                                                                      *
 * This file is part of Cofy.                                           *
 *                                                                      *
 * Cofy is free software; you can redistribute it and/or modify         *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.                                                   *
 *                                                                      *
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                 *
 * See the  GNU General Public License for more details.                *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program; if not, write to the Free Software          *
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.                                                                 *
 ************************************************************************/
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2024 by SI-4YOU ({@link https://www.siforyou.com})
 */

use Capwelton\LibOrm\Criteria\ORMCriteria;
use Capwelton\LibOrm\Field\ORMBoolField;
use Capwelton\LibOrm\Field\ORMIntField;
use Capwelton\LibOrm\FuncLibOrm;
use Capwelton\LibOrm\MySql\ORMMySqlRecordSet;
use Capwelton\LibOrm\MySql\ORMMySqlBackend;
use Capwelton\LibOrm\ORMIterator;
use Capwelton\LibOrm\ORMRecordSet;
use Capwelton\LibOrm\ORMRecord;
use Cofy\Utilities\FunctionalityTools\BabFunctionality;

use function Capwelton\LibOrm\ORM_IntField;
use function Capwelton\LibOrm\ORM_BoolField;

require_once "base.php";
require_once dirname(__FILE__) . '/../functions.php';

global $babDB;

/** @var FuncLibOrm $LibOrm */
$LibOrm = BabFunctionality::get('LibOrm');

$LibOrm->initMysql();
ORMMySqlRecordSet::setBackend(new ORMMySqlBackend($babDB));

/**
 * @method authldap_SiteOption[]|ORMIterator select(ORMCriteria $criteria)
 * @method authldap_SiteOption    get(mixed $criteria)
 * @method authldap_SiteOption    request(mixed $criteria)
 * @method authldap_SiteOption    newRecord()
 * 
 * @property ORMIntField    $site
 * @property ORMBoolField   $allowAdmin
 * @property ORMBoolField   $notifyGroups
 * @property ORMBoolField   $checkFields
 * 
 * @inheritdoc  ORMRecordSet
 * @see         ORMRecordSet
 */
class authldap_SiteOptionSet extends ORMRecordSet
{

    public function __construct()
    {
        parent::__construct();

        $this->setPrimaryKey('id');

        $this->addFields(
            ORM_IntField('site')->setDescription('The id of the site the options are for'),
            ORM_BoolField('allowAdmin')->setDescription('Allow administrators to log in if the LDAP/AD authentication fails'),
            ORM_BoolField('notifyGroups')->setDescription('Wether or not the selected groups are notified when a new user is created'),
            ORM_BoolField('checkFields')->setDescription('Wether or not the givenname, lastname and email are tested when creating a new user')
        );
    }
}


/**
 * @method authldap_SiteOptionSet   getParentSet()
 * 
 * @property int    $site
 * @property bool   $allowAdmin
 * @property bool   $notifyGroups
 * @property bool   $checkFields
 * 
 * @inheritdoc  ORMRecord
 * @see         ORMRecord
 */
class authldap_SiteOption extends ORMRecord {}

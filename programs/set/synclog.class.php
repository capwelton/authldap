<?php

/************************************************************************
 * COFY https://www.siforyou.com                                         *
 ************************************************************************
 * Copyright (c) 2023 by Si-4You ( https://www.siforyou.com )            *
 *                                                                      *
 * This file is part of Cofy.                                           *
 *                                                                      *
 * Cofy is free software; you can redistribute it and/or modify         *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.                                                   *
 *                                                                      *
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                 *
 * See the  GNU General Public License for more details.                *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program; if not, write to the Free Software          *
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.                                                                 *
 ************************************************************************/
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2024 by SI-4YOU ({@link https://www.siforyou.com})
 */

use Capwelton\LibOrm\Criteria\ORMCriteria;
use Capwelton\LibOrm\Field\ORMDatetimeField;
use Capwelton\LibOrm\Field\ORMEnumField;
use Capwelton\LibOrm\Field\ORMFkField;
use Capwelton\LibOrm\FuncLibOrm;
use Capwelton\LibOrm\MySql\ORMMySqlRecordSet;
use Capwelton\LibOrm\MySql\ORMMySqlBackend;
use Capwelton\LibOrm\ORMIterator;
use Capwelton\LibOrm\ORMRecordSet;
use Capwelton\LibOrm\ORMRecord;
use Cofy\Utilities\FunctionalityTools\BabFunctionality;

use function Capwelton\LibOrm\ORM_EnumField;
use function Capwelton\LibOrm\ORM_DatetimeField;

require_once "base.php";
require_once dirname(__FILE__) . '/../functions.php';

global $babDB;

/** @var FuncLibOrm $LibOrm */
$LibOrm = BabFunctionality::get('LibOrm');

$LibOrm->initMysql();
ORMMySqlRecordSet::setBackend(new ORMMySqlBackend($babDB));

/**
 * @method authldap_SyncLog[]|ORMIterator select(ORMCriteria $criteria)
 * @method authldap_SyncLog    get(mixed $criteria)
 * @method authldap_SyncLog    request(mixed $criteria)
 * @method authldap_SyncLog    newRecord()
 * @method authldap_ServerSet  server()
 * 
 * @property ORMDatetimeField   $createdOn
 * @property ORMDatetimeField   $modifiedOn
 * @property ORMEnumField       $status
 * @property ORMFkField         $server
 * 
 * @inheritdoc  ORMRecordSet
 * @see         ORMRecordSet
 */
class authldap_SyncLogSet extends ORMRecordSet
{
    public function __construct()
    {
        parent::__construct();

        $this->setPrimaryKey('id');

        $this->addFields(
            ORM_DatetimeField('createdOn'),
            ORM_DatetimeField('modifiedOn'),
            ORM_EnumField('status', authldap_SyncLog::getStatuses())
        );
        $this->hasOne('server', 'authldap_ServerSet')->setDescription('The server id');
    }
}

/**
 * @method authldap_SyncLogSet  getParentSet()
 * @method authldap_Server      server()
 * 
 * @property string $createdOn
 * @property string $modifiedOn
 * @property int    $status
 * @property int    $server
 * 
 * @inheritdoc  ORMRecord
 * @see         ORMRecord
 */
class authldap_SyncLog extends ORMRecord
{
    const SYNCLOG_BEGIN = 0;
    const SYNCLOG_ONPROGRESS = 1;
    const SYNCLOG_FINISHED = 2;

    public static function getStatuses()
    {
        return [
            self::SYNCLOG_BEGIN => authldap_translate('Begin'),
            self::SYNCLOG_ONPROGRESS => authldap_translate('Onprogress'),
            self::SYNCLOG_FINISHED => authldap_translate('Finished')
        ];
    }

    public function getStatus()
    {
        switch ($this->status) {
            case self::SYNCLOG_BEGIN:
                return  authldap_translate('Begin');
                break;
            case self::SYNCLOG_ONPROGRESS:
                return  authldap_translate('Onprogress');
                break;
            case self::SYNCLOG_FINISHED:
                return  authldap_translate('Finished');
                break;
        }
        return authldap_translate("status");
    }
}

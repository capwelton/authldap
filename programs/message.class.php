<?php

/************************************************************************
 * COFY https://www.siforyou.com                                         *
 ************************************************************************
 * Copyright (c) 2023 by Si-4You ( https://www.siforyou.com )            *
 *                                                                      *
 * This file is part of Cofy.                                           *
 *                                                                      *
 * Cofy is free software; you can redistribute it and/or modify         *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.                                                   *
 *                                                                      *
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                 *
 * See the  GNU General Public License for more details.                *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program; if not, write to the Free Software          *
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.                                                                 *
 ************************************************************************/
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2024 by SI-4YOU ({@link https://www.siforyou.com})
 */

use function Cofy\Utilities\Addon\bab_toHtml;

abstract class authldap_Message
{
    abstract public function getTitle();

    abstract public function getTextMessage();

    public function getHtmlMessage()
    {
        return bab_toHtml($this->getTextMessage(), BAB_HTML_ALL);
    }
}


abstract class authldap_UserMessage extends authldap_Message
{

    public $iduser;

    /**
     * @var string
     */
    public $nickname;

    /**
     * @var string
     */
    public $lastname;

    /**
     * @var string
     */
    public $firstname;


    public function getTextMessage()
    {
        return $this->lastname . ' ' . $this->firstname;
    }
}


class authldap_RegisterUserMessage extends authldap_UserMessage
{
    public function getTitle()
    {
        return authldap_translate('Registered users');
    }
}

class authldap_DisableUserMessage extends authldap_UserMessage
{
    public function getTitle()
    {
        return authldap_translate('Disabled users');
    }
}

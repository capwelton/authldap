<?php

/************************************************************************
 * COFY https://www.siforyou.com                                         *
 ************************************************************************
 * Copyright (c) 2023 by Si-4You ( https://www.siforyou.com )            *
 *                                                                      *
 * This file is part of Cofy.                                           *
 *                                                                      *
 * Cofy is free software; you can redistribute it and/or modify         *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.                                                   *
 *                                                                      *
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                 *
 * See the  GNU General Public License for more details.                *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program; if not, write to the Free Software          *
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.                                                                 *
 ************************************************************************/
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2024 by SI-4YOU ({@link https://www.siforyou.com})
 */

use function Cofy\Utilities\Addon\bab_Widgets;
use Capwelton\Widgets\Widgets\Item\WidgetLink;
use Cofy\Utilities\Func\Icons\FuncIcons;

use function Cofy\Utilities\User\bab_getUserName;

class authldap_CtrlSyncLog extends authldap_Controller
{
    // not use, just in case
    public function displayList($id = null)
    {
        $W = bab_Widgets();
        $syncLogSet = authldap_SyncLogSet();
        $syncLogSet->server();
        $syncLogs = $syncLogSet->select()->orderDesc($syncLogSet->createdOn);

        $table = $W->TableArrayView();
        $table->setHeader(
            array(
                authldap_translate('Status'),
                authldap_translate('Date de synchronisation'),
                authldap_translate('Serveur'),
                authldap_translate('Liste User')
            )
        );

        $content = array();

        foreach ($syncLogs as $syncLog) {
            $row = array();

            $row[] = $W->Label($syncLog->status);
            $row[] = $W->Label($syncLog->createdOn);
            $row[] = $W->Label($syncLog->server->name);

            $listUser = $W->HBoxItems()->setHorizontalSpacing(1, 'em');
            $listUser->addItem(
                $W->Link(
                    authldap_translate('Voir'),
                    $this->proxy()->getListUser($syncLog->id)
                )->setIcon(FuncIcons::ACTIONS_VIEW_HISTORY)->setOpenMode(WidgetLink::OPEN_POPUP)
            );
            $row[] = $listUser;

            $content[] = $row;
        }

        $table->setContent($content);

        $table->setReloadAction($this->proxy()->displayList($table->getId()));

        return $table;
    }

    public function display($id, $itemId = null)
    {
        $W = bab_Widgets();
        $syncLogSet = authldap_SyncLogSet();
        $syncLogSet->server();

        $table = $W->TableArrayView();
        $table->setHeader(
            array(
                authldap_translate('Status'),
                authldap_translate('Date de synchronisation'),
                authldap_translate('Serveur'),
                authldap_translate('Liste User')
            )
        );

        $content = array();

        $record = $syncLogSet->get($id);
        if (!isset($record)) {
            return true;
        }

        $row = array();

        $row[] = $W->Label($record->getStatus());
        $row[] = $W->Label($record->createdOn);
        $row[] = $W->Label($record->server->name);

        $listUser = $W->HBoxItems()->setHorizontalSpacing(1, 'em');
        $listUser->addItem(
            $W->Link(
                authldap_translate('Voir'),
                $this->proxy()->getListUser($record->id)
            )->setIcon(FuncIcons::ACTIONS_VIEW_HISTORY)->setOpenMode(WidgetLink::OPEN_DIALOG)
        );
        $row[] = $listUser;

        $content[] = $row;

        $table->setContent($content);

        $table->setReloadAction($this->proxy()->display($id, $table->getId()));
        return $table;
    }

    public function getListUser($idSyncLog)
    {
        $userSyncSet = authldap_UserSyncSet();
        $userSyncs = $userSyncSet->select(
            $userSyncSet->syncLog->is($idSyncLog)
        );

        $W = bab_Widgets();

        $page = $W->Page();

        foreach ($userSyncs as $userSync) {
            $page->addItem($W->Html(bab_getUserName($userSync->user) . " => " . $userSync->getTypeOfAction()));
        }

        return $page;
    }
}

jQuery(document).ready(function(){
	var layout = jQuery('#adminPwdInputs');
	var meta = window.babAddonWidgets.getMetadata(layout.attr('id'));
	jQuery('#adminPwdInputs input').each(function(){
		jQuery(this).keyup(function(){
			if(jQuery('#adminPwdInput1').val() != jQuery('#adminPwdInput2').val()){
				console.log(false)
			}
			else{
				
			}
		    layout.find('.widget-inputpassword-rules').remove();
		    layout.append('<div class="widget-inputpassword-rules"></div>');
		    if(jQuery('#adminPwdInput1').val() != jQuery('#adminPwdInput2').val()){
		    	var ruleDiv = jQuery('<div class="widget-inputpassword-rule-wrong">'+meta.differentPwdMessage+'</div>');
		        jQuery(layout).find('.widget-inputpassword-rules').append(ruleDiv);
			}
		})
	});
});